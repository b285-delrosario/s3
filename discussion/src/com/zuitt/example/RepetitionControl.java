package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {

    //Loops
        //are control structures that allow code blocks to be executed multiple times

    public static void main(String[] args) {

        //While Loop
            //allows for repetitive use of code, similar to for loops, but are usually for situations where the content to iterate through is indefinite

        int x = 0; //initial value

        while(x<10) { //condition
            System.out.println("(While Loop) Loop Number: " + x++); //increment/decrement
        }

        //Do-while
            //similar to while loops
            //However, do-while loops will always execute at least once - while loops may not execute at all

        int y = 10;
        do {
            System.out.print("(This will run at least once) Countdown: " + y);
        } while (y>10);

        //For Loop
        //Syntax:
        /*
            for(initialVal;condition;iteration) {
                //code block
            }
        */

        //Mini-Activity
        //initial value of 0
        //i less than 10
        //increase every loop
        //print "(For Loop) Current Count: " + i);

        for(int i = 0; i < 10; i++) {
            System.out.print("(For Loop) Current Count: " + i + "\n");
        }

        //For Loop with Arrays
        int[] intArray = {100, 200, 300, 400, 500};
        for(int i = 0; i < intArray.length; i++ ) {
            System.out.println(intArray[i]);
        }

        //For-each loop with array
        /*

        Syntax:
            for(dataType itemName: arrayName) {
                //code block
            }

        */

        String[] boyBandArray = {"John", "Paul", "George", "Ringo"};
        for(String member: boyBandArray) {
                System.out.println(member);
        }

        //Multidimensional array
        String[][] classroom = new String[3][3];
        //[row][column]

        //First row
        classroom[0][0] = "Jenny";
        classroom[0][1] = "Liza";
        classroom[0][2] = "Rose";
        //Second row
        classroom[1][0] = "Ash";
        classroom[1][1] = "Misty";
        classroom[1][2] = "Brock";
        //Third row
        classroom[2][0] = "Amy";
        classroom[2][1] = "Lulu";
        classroom[2][2] = "Morgan";

        for(int row = 0; row < 3; row++) {
            for(int col = 0; col < 3; col++) {
                System.out.println("classroom [" + row  + "][" + col + "] = " + classroom[row][col]);
            }
        }

        //for each loop with multidimensional array
        for(String[] row: classroom) {
            for(String column: row) {
                System.out.println(column);
            }
        }//Jenny

        //Print as a matrix
        for(int row = 0; row < classroom.length; row++) {
            for(int column = 0; column < classroom[row].length; column++) {
                System.out.println(classroom[row][column] + " ");
            }
            System.out.println();
        }

        //Mini Activity
            //create an array and print as matrix
            //***
            //285
            //***

        String[][] act = {{"*", "*", "*"}, {"2", "8", "5"}, {"*", "*", "*"}};

        for(int i = 0; i < act.length; i++) {

            for(int j = 0; j < act[i].length; j++) {
                System.out.println(act[i][j] + " ");
            }
            System.out.println();
        }

        //for-each with ArrayList
        //Syntax:
        /*
        arrayListName.foreach(Consumer<E> -> //code block);
        "->" this is called the lambda - is used to separate parameter and implementation
        */

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);
        System.out.println("ArrayList: " + numbers);

        numbers.forEach(num -> System.out.println("ArrayList: " + num));

        //foreach with hashmaps
        //Syntax:
        /*
        hashMapName.foreach((key, value) -> //code block);
        */

        HashMap<String, Integer> grades = new HashMap<String, Integer>() {
            {
                this.put("English", 90);
                this.put("Math", 95);
                this.put("Science", 97);
                this.put("History", 94);
            }
        };
        grades.forEach((subject, grade)-> {
            System.out.println("Hashmaps: " + subject + " : " + grade + "\n");
        });

    }
}
