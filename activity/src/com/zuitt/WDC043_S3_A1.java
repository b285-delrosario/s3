package com.zuitt;

import java.util.Scanner;

public class WDC043_S3_A1 {
    public static void main(String[] args) {
        Scanner activity = new Scanner(System.in);

        int integer = 0;

        System.out.println("Input an integer whose factorial will be computed:");

        try {
            integer = activity.nextInt();
        }

        catch (Exception e) {

            System.out.println("Invalid input!");
            e.printStackTrace();
        }

        finally {
            if (integer < 0) {
                System.out.println("The factorial can only be computed for positive integers.");
            }
            else {
                int num = 1;
                int i = 1;
                while(i <= integer && integer > 0) {
                    num *= i;
                    i++;
                }
                System.out.println("While loop: Factorial of "+ integer + " is " + num);

                num = 1;
                for(i = 1; i <= integer; i++) {
                    num *= i;
                }
                System.out.println("For loop: Factorial of "+ integer + " is " + num);
            }
        }
    }
}
